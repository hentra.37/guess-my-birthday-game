from random import randint
player_name = input("What is your name?\n")
#input lets someone respond to the prompt and assign that response to player_name
#\n lets your input start on a new line
for guess_birthday in range(1, 6):
    #limits guesses to 5
    month_guess = randint(1, 12)
    #12 months in a year
    year_guess = randint(1922, 2022)
    #Maybe a newborn will want to play
    print("Guess", guess_birthday, ":", player_name, "were you born in", month_guess,
    "/", year_guess)
    #remember to put commas(?)

    response = input("Yes or No?\n")
    if response == "Yes":
        print("I knew it!")
        exit()
    elif guess_birthday == 5:
        print("I have other things to do. Good bye.")
        #when guesses reach 5 this will output
    else:
        print("Drat! Lemme try again!")